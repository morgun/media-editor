'use strict';
window.secScale = 30; //pixels
window._Frame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame;
window.console = console || {log:function(){}, warn: function(){}};

/* Public interface */
!function(){
	var tracks = [],
		volume = 1.0;

	window.TimeLine = {
		Track: function(/*File*/ source) {
			var file = new MediaFile(source);

			Object.defineProperty(this, 'duration', {
				set: function(value) {
					file.duration(value);
				},
				get: function() {
					file.duration();
				},
				enumerable: true
			});

			Object.defineProperty(this, 'offset', {
				set: function(value) {
					file.offset(value);
				},
				get: function() {
					file.offset();
				},
				enumerable: true
			});

			Object.defineProperty(this, 'file', {
				get: function() {
					return file;
				},
				enumerable: false
			});

			return this; // For lalkazZ :)
		},
		add: function(/* Track */ track) {
			viewModel.timeline.push(track.file);
			tracks.push(track);
		},
		remove: function(/* Track */ track) {
			viewModel.timeline.remove(track.file);
			tracks.splice(tracks.indexOf(track), 1);
		},
		play: function() {
			video.syncedPlayback.play();
		},
		pause: function() {
			video.syncedPlayback.pause();
		}
	};

	Object.defineProperty(TimeLine, 'tracks', {
		get: function() {
			return tracks;
		},
		enumerable: true
	});

	Object.defineProperty(TimeLine, 'volume', {
		set: function(value) {
			viewModel.volume(value);
		},
		get: function() {
			return  viewModel.volume();
		},
		enumerable: true
	});

	Object.defineProperty(TimeLine, 'position', {
		set: function(value) {
			var played = viewModel.syncedPlayback.playing();

			if (played) viewModel.syncedPlayback.pause();
			viewModel.timeline.position(Math.min(value, viewModel.timeline.totalDuration()));
			if (played)
				viewModel.syncedPlayback.play();
			else
				viewModel.syncedPlayback.sync(viewModel.syncedPlayback.draw);
		},
		get: function() {
			return viewModel.timeline.position();
		},
		enumerable: true
	});
}();

!function(){
	var targetCanvas,
		ctx,
		thumbSize = {w: 80, h: 45},
		tmpVideo,

		/** Funcitons **/
		drawThumbnails = function(track) {
			var element = track.element,
				copy = document.createElement('video'),
				offscreen = document.createElement('canvas'),
				context = offscreen.getContext("2d"),
				thumbs = 0,
				step = 80 / secScale,
				frame = 0,
				draw = function() {
					context.drawImage(copy, thumbSize.w * frame, 0, thumbSize.w, thumbSize.h);
					track.thumb(offscreen.toDataURL('image/jpeg', 0.4));
					frame++;
					if (frame < thumbs) {
						copy.currentTime += step;
					} else {
						// all frames drawed
					}
				}; //Will be used as max

			copy.autoplay = false;
			copy.addEventListener('loadedmetadata', function() {
				thumbs = Math.max(1, 1 + copy.duration/step|0); //Number of frames
				offscreen.width = thumbSize.w * thumbs;
				offscreen.height = thumbSize.h;
				context.globalAlpha = 0.5;
			});
			copy.addEventListener('loadeddata', draw);
			copy.addEventListener('seeked', draw);
			copy.src = element.src;
		},

		toggleFullscreen = function(element) {};

	if ('fullscreenElement' in document) {
		console.log('Standards fullscreen API');
		toggleFullscreen = function(element) {
			if (document.fullscreenElement) {
				document.cancelFullScreen();
			} else {
				element.requestFullscreen();
			}
		};
	}

	if ('webkitFullscreenElement' in document) {
		console.log('Webkit fullscreen API');
		toggleFullscreen = function(element) {
			if (document.webkitFullscreenElement) {
				document.webkitCancelFullScreen();
			} else {
				element.webkitRequestFullscreen();
			}
		};
	}

	if ('mozFullScreenElement' in document) {
		console.log('Mozilla fullscreen API');
		toggleFullscreen = function(element) {
			if (document.mozFullScreenElement) {
				document.mozCancelFullScreen();
			} else {
				element.mozRequestFullScreen();
			}
		};
	}

	if ('msFullscreenElement' in document) {
		console.log('MS fullscreen API');
		toggleFullscreen = function(element) {
			if (document.msFullscreenElement) {
				document.msCancelFullScreen();
			} else {
				element.msRequestFullscreen();
			}
		};
	}


	window.viewModel = {
		timeline: ko.observableArray(),
		volume: ko.observable(1.0),
		syncedPlayback: {
			playing: ko.observable(false),
			timeouts: [],
			start: {
				position: 0,
				ts: 0
			},
			draw: function() {
				var position = viewModel.timeline.position();
				targetCanvas.width = targetCanvas.width;
				 //Fast clear

				viewModel.timeline().forEach(function(track) {
					var element = track.element;
					if (track.visible() && (element.tagName == 'IMG' || element.tagName == 'VIDEO')) {
						ctx.globalAlpha = 1 - Math.min(1, Math.max((track.fadeInTS - position) / track.fadeDuration, (position - track.fadeOutTS) / track.fadeDuration, 0));
						ctx.drawImage(element, 0, 0, targetCanvas.width, targetCanvas.height);
					}
				});


				if (this.playing()) {
					viewModel.timeline.position(this.start.position + (Date.now() - this.start.ts) / 1000);
					_Frame(this.draw);
					if (viewModel.timeline.position() > viewModel.timeline.totalDuration()) {
						this.stop();
					}
				}
			},
			toggle: function(){
				this.playing() ? this.pause() : this.play();
			},
			pause: function() {
				console.log('Pause');
				this.timeouts.forEach(function(t){clearTimeout(t);});
				this.timeouts = [];
				viewModel.timeline().forEach(function(track){
					var element = track.element;
					if (element.pause) {
						element.pause();
					}
				});
				this.playing(false);
			},
			stop: function() {
				this.pause();
				viewModel.timeline.position(0.0);
				viewModel.syncedPlayback.sync(viewModel.syncedPlayback.draw);
			},
			sync: function(syncedCallback) {
				var all = [],
					position = viewModel.timeline.position();

				viewModel.timeline().forEach(function(track){
					var element = track.element;
				    if (element.defer) {
					    all.push(element.defer = when.defer());
					    element.currentTime = Math.max(0, position - track.start);
				    }
				});

				when.all(all).then(syncedCallback);
			},
			play: function() {
				var self = this,
					position = viewModel.timeline.position();
				this.sync(function() {
					viewModel.timeline().forEach(function(track){
						var element = track.element,
							start = track.start,
							end = track.end;

						if (!element.play || position >= end) return; //Skipping images and non drawable

						if (position >= start) {
							element.play();
						} else {
							self.timeouts.push(setTimeout(function(){
								element.play();
							}, (start - position) * 1000));
						}

						self.timeouts.push(setTimeout(function(){
							element.pause();
						}, (end - position) * 1000));
					});
					console.log('Play');
					self.start = {
						position: position,
						ts: Date.now()
					};
					self.playing(true);
					self.draw();
				});
 			}
		},
		drop: {
			active: ko.observable(false),
			position: ko.observable()
		},
		toggle: function(track, evt) {
			evt.preventDefault();
			this(!this()); //Black magic
			if (!viewModel.syncedPlayback.playing())
				viewModel.syncedPlayback.draw(); //redraw on pause
			return false;
		},
		moveDown: function(track, evt) {
			var x = evt.clientX,
				offset = track.offset(),
				played = viewModel.syncedPlayback.playing(),
				move = function(event) {
					track.offset(Math.max(0, offset + (event.clientX - x) / secScale));
					evt.stopPropagation();
				},
				up = function() {
					if (played) viewModel.syncedPlayback.play();
					document.removeEventListener('mouseup', up);
					document.removeEventListener('mousemove', move);
				};

			if (played) viewModel.syncedPlayback.pause();

			document.addEventListener('mouseup', up, false);
			document.addEventListener('mousemove', move, false);
			evt.stopPropagation();
			evt.preventDefault();
		},
		cutDown: function(track, evt) {
			var x = evt.clientX,
				duration = track.duration(),
				maxDuration = track.maxDuration() || Infinity,
				played = viewModel.syncedPlayback.playing(),
				move = function(event) {
					track.duration(Math.min(maxDuration, Math.max(0, duration + (event.clientX - x) / secScale)));
					event.stopPropagation();
				},
				up = function() {
					if (played) viewModel.syncedPlayback.play();
					document.removeEventListener('mouseup', up);
					document.removeEventListener('mousemove', move);
				};

			if (played) viewModel.syncedPlayback.pause();

			document.addEventListener('mouseup', up, false);
			document.addEventListener('mousemove', move, false);
			evt.stopPropagation();
			evt.preventDefault();
		},
		removeClick: function(track, evt) {
			viewModel.timeline.remove(track);
			viewModel.syncedPlayback.draw();
			evt.stopPropagation();
			evt.preventDefault();
		},
		volumeClick: function(model, evt) {
			viewModel.volume(1 - evt.pageY/evt.currentTarget.offsetHeight);
			evt.stopPropagation();
			evt.preventDefault();
		},
		positionClick: function(model, evt) {
			var played = viewModel.syncedPlayback.playing();

			if (played) viewModel.syncedPlayback.pause();

			viewModel.timeline.position(Math.max(0, Math.min((evt.clientX - 5) / secScale, viewModel.timeline.totalDuration())));

			if (played) {
				viewModel.syncedPlayback.play();
			} else {
				viewModel.syncedPlayback.sync(viewModel.syncedPlayback.draw);
			}

			evt.stopPropagation();
			return true;
		}
	};

	viewModel.timeline.subscribe(function(changes){
		changes.forEach(function(change){
			if (!change.value.element) {
				viewModel.timeline.remove(change.value);
				console.warn('Skipped unsupported file');
			}
		});
	}, viewModel.timeline, 'arrayChange');

	viewModel.syncedPlayback.draw = viewModel.syncedPlayback.draw.bind(viewModel.syncedPlayback);

	viewModel.timeline.position = ko.observable(0.0);

	viewModel.timeline.totalDuration = ko.computed(function() {
		return viewModel.timeline().reduce(function(result, value) {
			value.duration();
			return Math.max(result, value.end);
		}, 0);
	});

	viewModel.volume.subscribe(function(newVolume){
		viewModel.timeline().forEach(function(track){
			track.element.volume = newVolume;
		});
	});

	window.MediaFile = function (file) {
		var self = this,
			type = file.type.split('/')[0],
			element;
		Object.defineProperty(this, 'source', { get: function() { return file; }, enumerable: true });
		Object.defineProperty(this, 'type', { get: function() { return type; }, enumerable: true });
		Object.defineProperty(this, 'element', { get: function() { return element; }, enumerable: true });

		self.url = URL.createObjectURL(file);
		self.offset = ko.observable(0.0);
		self.duration = ko.observable(null);
		self.maxDuration = ko.observable(null);
		self.maxDuration.subscribe(function(newValue) {
			if (self.duration() === null) {
				self.duration(newValue);
			}
		});
		self.thumb = ko.observable();
		self.fadein = ko.observable(true);
		self.fadeout = ko.observable(false);
		self.visible = ko.computed(function() {
			var pos = viewModel.timeline.position() - self.offset();
			return pos >= 0 && pos < self.duration();
		});

		ko.computed(function(){
			var offset = self.offset(),
				duration = self.duration();

			self.start = offset;
			self.end = offset + duration;
			self.fadeDuration = Math.min(1, duration/2);
			self.fadeInTS = self.start + self.fadeDuration * self.fadein();
			self.fadeOutTS = self.end - self.fadeDuration * self.fadeout();
		});

		switch(self.type) {
			case 'audio':
				self.thumb('img/audio.png');
			case 'video':
				element = document.createElement(self.type);
				if (!element.canPlayType(file.type)) { //skipping unsupported formats
					element = null;
					break;
				}
				element.autoplay = false;
				element.defer = when.defer(); //for seeked callback
				element.addEventListener('loadedmetadata', function() {
					self.maxDuration(element.duration);
					if (self.type == 'video') {
						element.width = Math.floor(element.videoHeight / 9 * 16);
						element.height = element.videoHeight;
						drawThumbnails(self);
					}
				});
				element.addEventListener('seeked', function() {
					element.defer.resolve();
				});
				element.volume = viewModel.volume();
				element.src = self.url;
				break;
			case 'image':
				element = document.createElement('img');
				element.src = self.url;
				self.thumb(self.url);
				self.duration(3.0); //default 3seconds duration for Image
				break;
		}

		if (!element) {
			console.warn('Unsupported MIME type: ' + file.type);
		}
	};

	document.addEventListener("DOMContentLoaded", function() {
		tmpVideo = document.createElement('video');
		targetCanvas = document.getElementById('result');
		ctx = targetCanvas.getContext('2d');
		ko.applyBindings(viewModel);
	});

	document.addEventListener('keydown', function(evt){
		switch (evt.which) {
			case 32: // Space
				viewModel.syncedPlayback.toggle();
				evt.preventDefault();
				break;
			case 70: // F
				toggleFullscreen(targetCanvas);
				break;
			case 187: case 107: // +
				viewModel.volume(Math.min(1, viewModel.volume() + .1));
				break;
			case 189: case 109: // -
				viewModel.volume(Math.max(0, viewModel.volume() - .1));
				break;
			default:
				//console.log(evt.which);
				break;
		}
	});

	document.addEventListener('drop', function (evt) {
		evt.preventDefault();
		viewModel.drop.active(false);
		console.log(evt.dataTransfer.files);
		Array.prototype.forEach.call(evt.dataTransfer.files, function(file) {
			var type = file.type.split('/')[0],
				track;


			track = new MediaFile(file);
			track.offset(viewModel.drop.position());
			viewModel.timeline.push(track);
		});
	}, true);

	document.addEventListener('dragover', function(evt) {
		evt.preventDefault();
		evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
		viewModel.drop.active(true);
		viewModel.drop.position(Math.max(0, (evt.pageX - 5)/secScale));
	}, false);

}();
